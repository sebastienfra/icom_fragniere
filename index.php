<!doctype html>
<?php require_once('inc/config.php'); ?>

<?php
$stmt = $db->prepare("SELECT * FROM project");
$stmt->execute();
$tasks= $stmt->fetchAll();
?>

<html class="no-js" lang="en">
  <?php require_once('template/head.php'); ?>
  <body>
    <?php require_once('template/header.php'); ?>

 <ul class="project_management">
            <li class="header">
              <ul class="month">
                <li>01</li>
                <li>02</li>
                <li>03</li>
                <li>04</li>
                <li>05</li>
                <li>06</li>
                <li>07</li>
                <li>08</li>
                <li>09</li>
                <li>10</li>
                <li>11</li>
                <li>12</li>
              </ul>
            </li>
            <li>
            <?php foreach( $data as $row) :?>
                  <span class="project_id"><?php echo $row['id']?></span>
                  <span class="project_name"><?php echo $row['mane']?></span>
                  <span class="project_date_start"><?php echo $row['start']?></span>
                  <span class="project_date_end"><?php echo $row['end']?></span>
                  <a href="delete.php?id=<?php echo $row['id']?>" class="taskliste_item_delete"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                </li>
            <?php endforeach; ?> 

    </ul>
  </main>
</div>
    <?php require_once('template/footer.php'); ?>
  </body>
</html>
