<!doctype html>

<?php
require_once('inc/config.php');

$modify = FALSE;

$current_date = date('Y-m-d', strtotime('+1 day'));


if(isset($_REQUEST['id'])){
  $id = $_REQUEST['id'];
  $modify = TRUE;
  $stmt = $db->prepare($q_edit);
  $stmt->execute(array($id));
  $tasks = $stmt->fetchAll();


  $name = $tasks[0]['name'];
  $start = $tasks[0]['start'];
  $end = $tasks[0]['end'];
}
?>

<html class="no-js" lang="en">
    <?php require_once('template/head.php'); ?>
    <body>
      <?php require_once('template/header.php'); ?>

<div class="off-canvas-wrapper">
  <div class="off-canvas position-left" id="offCanvasLeft" data-transition="push" data-off-canvas>
    <?php require_once('template/offcanvas.php'); ?>
  </div>

  <main class="off-canvas-content main" data-off-canvas>



        <form class="form-edit" method="post" action="update.php">
          <ul>
              <input id="id" name="id" type="hidden" value="<?php echo $modify?$id:''; ?>"/>
            <li class="row medium-6 large-4 columns">
              <label for="start">Start date</label>
              <input class="form-edit-input" name="start"  id="start" value="<?php echo $modify?$start:'0'; ?>" min="0" max="10" place type="number"/>
            </li>
            <li class="row medium-6 large-4 columns">
              <label for="end">Due date</label>
              <input class="form-edit-input" name="end"  id="end" value="<?php echo $modify?$end:'0'; ?>" min="0" max="10" place type="number"/>
            </li>
            <li class="row medium-6 large-4 columns">
              <label for="name">name</label>
              <textarea class="form-edit-input" name="name"  id="name" autofocus required><?php echo $modify?$name:''; ?></textarea>
            </li>
            <li class="row medium-6 large-4 columns">
              <input class="form-edit-input submit"  type="submit"  value="Submit">
            </li>
          </ul>
        </form>


      </main>
      </div>
      <?php require_once('template/footer.php'); ?>
    </body>
</html>
